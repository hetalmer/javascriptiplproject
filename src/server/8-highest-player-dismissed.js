//Find highest number of times one player has been dismissed by another player
function playerDismissedHighest(deliveries) {
    //get all the data from deliveries which contain the player dismissed
    const highestDismissed = deliveries.filter(values=>values.player_dismissed !== '').map(value=>{
        return{
            bowler : value.bowler,
            player_dismissed : value.player_dismissed
        };
    });

    //combine player dismissed and bowler and count how many times dismissed
    const topHightestDismissed = highestDismissed.reduce((result,value)=>{
        key = value.player_dismissed + " by " + value.bowler;
        if(result.hasOwnProperty(key)){
            result[key] += 1
        }
        else{
            result[key] = 1;
        }
        return result;
    },{});
    maxVal =Math.max.apply(Math,Object.values(topHightestDismissed)); // get the maximum time dismissed
    return Object.fromEntries(Object.entries(topHightestDismissed).filter(value => value[1] === maxVal)); //  return only maximum times dismissed
}
module.exports = playerDismissedHighest;