//Program to find the strike rate of a batsman for each season
//function getId returns the data of Id as array by season
function getId(matches) {
	const idAndYear = matches.reduce((result, value) => {
		if (result.hasOwnProperty(value.season)) {
			result[value.season].push(parseInt(value.id));
		} else {
			result[value.season] = [parseInt(value.id)];
		}
		return result;
	}, {});
	return idAndYear;
}
function findStrikeRate(matches, deliveries) {
	const finalData = Object.entries(matches).reduce((result, values) => {
		//reduce by matches which return yar and ids
		const year = values["0"];
		dataForYear = deliveries.filter((value) => {
			//get the ids of every year one by one
			if (values["1"].includes(parseInt(value.match_id))) {
				return value;
			}
		});
		dataForYear.forEach((element) => {
			// get the data of a specific year and store it into object
			if (result.hasOwnProperty(year)) {
				if (result[year].hasOwnProperty(element.batsman)) {
					result[year][element.batsman][0] += parseInt(element.total_runs);
					result[year][element.batsman][1] += 1;
				} else {
					result[year] = Object.assign({}, result[year], {
						[element.batsman]: [parseInt(element.total_runs), 1],
					});
				}
			} else {
				result[year] = { [element.batsman]: [parseInt(element.total_runs), 1] };
			}
		});
		result[year] = Object.entries(result[year]).map((values) => {
			return {
				[values[0]]: Number(((values[1][0] / values[1][1]) * 100).toFixed(2)), // map the object by calculating its strike rate
			};
		});
		return result;
	}, {});
	return finalData;
}
module.exports = { getId, findStrikeRate };
