//Find the number of matches played per year and per month
function playerPerYearAndMonth(matches) {
	const finalData = matches.reduce((result, values) => {
		month = values.date.split("-")[1];
		if (result.hasOwnProperty(values.season)) {
			if (result[values.season].hasOwnProperty(month)) {
				result[values.season][month] += 1;
			} else {
				result[values.season] = Object.assign({}, result[values.season], {
					[month]: 1,
				});
			}
		} else {
			result[values.season] = { [month]: 1 };
		}
		return result;
	}, {});
	return finalData;
}

module.exports = playerPerYearAndMonth;
