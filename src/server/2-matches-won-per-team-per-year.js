//Number of Matches Won Per team per year
function matchesWonPerTeam(matches) {
    const numOfWinner = matches.reduce((result, data) => {
        const year = data.season; //get the year from data
        const winner = data.winner; // get the winner from the data
        if (result.hasOwnProperty(year)) {  // count winner based on on team by year
            if (result[year].hasOwnProperty(winner)) {//If result contain that property it increment by 1
                result[year][winner] += 1;
            }
            else {
                result[year][winner] = 1;// If result not contain property it is intitalized by 1
            }
        }
        else {
            result[year] = { [winner]: 1 }// If year also not in result property
        }
        return result;
    }, {});
    return numOfWinner;
}
module.exports = matchesWonPerTeam;