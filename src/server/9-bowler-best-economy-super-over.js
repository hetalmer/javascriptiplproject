//Find the bowler with the best economy in super overs

function superOverEconomy(data) {
    //get all the data with super over
    const superOverData = data.filter(values => values.is_super_over === '1');
    //get the field of bowler and total runs
    const dataForSuperOver = superOverData.map((data)=>{
        return {bowler : data.bowler,
            total_runs:parseInt(data.total_runs)};
    });
    // find the total runs and total balls by bowler
    const dataForBowler = dataForSuperOver.reduce((result,value)=>{
        if(result.hasOwnProperty(value.bowler)){
            result[value.bowler]['total_runs'] += value.total_runs;
            result[value.bowler]['balls'] += 1 
        }
        else{
            result[value.bowler] = {'total_runs':value.total_runs,'balls':1};
        }
        return result;
    },{});
    // calculate average for each bowler then sort it and return the first bowler
    const economyBowler = Object.entries(dataForBowler).map(value=>{
        return {bowler:value[0],
                average:value[1]['total_runs']/Math.ceil(value[1]['balls']/6)};
    }).sort((list1,list2)=> list1['average']-list2['average'])[0];
    return economyBowler;
}
module.exports = superOverEconomy;