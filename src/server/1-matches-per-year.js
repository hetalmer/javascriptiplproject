//Function for Number of Matches played per year for all IPL matches
function numberOfMatchesByYear(matches) {
    const yearList = matches.map(value => value.season); // Gourp by year so get all the years in array
    const numOfMaches = yearList.reduce((result, year) => { // reduced it by year and count how many matches played
        if (result.hasOwnProperty(year)) {
            result[year] += 1;
        }
        else {
            result[year] = 1;
        }
        return result;
    }, {});
    return numOfMaches; // return number of matches
}
module.exports = numberOfMatchesByYear;