///3. Find the top 10 bowlers who have conceded most extra runs
function extraRunsBowler(deliveris) {
	const finalData = deliveris.reduce((result, values) => {
		if (result.hasOwnProperty(values.bowler)) {
			result[values.bowler] += parseInt(values.extra_runs);
		} else {
			result[values.bowler] = parseInt(values.extra_runs);
		}
		return result;
	}, {});
	return Object.fromEntries(
		Object.entries(finalData)
			.sort((list1, list2) => list2[1] - list1[1])
			.slice(1, 11),
	);
}
module.exports = extraRunsBowler;
