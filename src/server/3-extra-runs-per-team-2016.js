//Extra runs conceded per team in the year 2016
//function to get the id value from matches file which season is year or 2016
function getId(matches, year) {
	const dataOfYear = matches.filter((result) => result.season === year);
	const idsOfYear = dataOfYear.map((result) => parseInt(result.id));
	return idsOfYear;
}
// first calculate allExtraRuns than after groupby it into by team
function extraRunsByTeam(matches, deliveries) {
	const allExtraRuns = deliveries.filter((value) => {
		return matches.includes(parseInt(value.match_id));
	});
	const extraRuns = allExtraRuns.reduce((result, values) => {
		//count extra runs by team
		const team = values.batting_team;
		const extra_runs = values.extra_runs;
		if (result.hasOwnProperty(team)) {
			result[team] += parseInt(extra_runs);
		} else {
			result[team] = parseInt(extra_runs);
		}
		return result;
	}, {});
	return extraRuns;
}
module.exports = { getId, extraRunsByTeam };
