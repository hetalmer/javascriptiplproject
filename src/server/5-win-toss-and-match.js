//Find the number of times each team won the toss and also won the match
//function to find how many times team win the toss and also win the match
function teamWinToassAndMatch(matches) {
	const winTossAndMatch = matches
		.filter((value) => value.winner === value.toss_winner)
		.map((value) => {
			// filter data with two field winner and toss winner
			const data = {
				winner: value.winner,
				toss_winner: value.toss_winner,
			};
			return data;
		});
	const result = winTossAndMatch.reduce((result, data) => {
		// Group by data by team and count number of times it wins.
		if (result.hasOwnProperty(data.winner)) {
			result[data.winner] += 1;
		} else {
			result[data.winner] = 1;
		}
		return result;
	}, {});
	return result;
}
module.exports = teamWinToassAndMatch;
