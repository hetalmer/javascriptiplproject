//4. Find the player who has hit the most number of boundaries i.e 4 runs for each season
function moreBoundries(matches, deliveries) {
	const idByYear = matches.reduce((result, value) => {
		if (result.hasOwnProperty(value.season)) {
			result[value.season].push(value.id);
		} else {
			result[value.season] = [value.id];
		}
		return result;
	}, {});
	const finalResult = Object.keys(idByYear).reduce((result, values) => {
		const seasonData = deliveries.filter((value) => {
			if (
				idByYear[values].includes(value.match_id) &&
				value.batsman_runs === "4"
			) {
				return value;
			}
		});
		seasonData.forEach((element) => {
			if (result.hasOwnProperty(values)) {
				if (result[values].hasOwnProperty(element.batsman)) {
					result[values][element.batsman] += 1;
				} else {
					result[values] = Object.assign({}, result[values], {
						[element.batsman]: 1,
					});
				}
			} else {
				result[values] = { [element.batsman]: 1 };
			}
		});
		return result;
	}, {});
	return Object.entries(finalResult).map((values) => {
		values[1] = Object.fromEntries(
			Object.entries(values[1])
				.sort((list1, list2) => list2[1] - list1[1])
				.slice(1, 2),
		);
		return values;
	});
}
module.exports = moreBoundries;
