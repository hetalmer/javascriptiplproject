//Program to calculate top 10 economical bowler in year 2015
function top10Bowler(matches, deliveries) { // function to find top10 economy bowler
    //get all the data which matches with match id
    const bowler = deliveries.map(values=>{
        if(matches.filter(val=>val===values.match_id)){
            return{
                bowler:values.bowler,
                total_runs : parseInt(values.total_runs)}
            };
        });
    //get the bowler data with its total runs and number of balls
    const dataForBowler = bowler.reduce((result,value)=>{
        if(result.hasOwnProperty(value.bowler)){
            result[value.bowler]['total_runs'] += value.total_runs;
            result[value.bowler]['balls'] += 1 
        }
        else{
            result[value.bowler] = {'total_runs':value.total_runs,'balls':1};
        }
        return result;
    },{});
    // calculate average for each bowler then sort it and return the top 10 bowler
    const economyBowler = Object.entries(dataForBowler).map(value=>{
        return {bowler:value[0],
                average:value[1]['total_runs']/Math.ceil(value[1]['balls']/6)};
    }).sort((list1,list2)=> list1['average']-list2['average']).slice(1,11);
    return economyBowler;
}
module.exports = top10Bowler;