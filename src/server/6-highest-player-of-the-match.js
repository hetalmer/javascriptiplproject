//Program to find highest number of player of the match award for each season
function highestPlayer(matches) {
    const data = matches.reduce((result, value) => { // It store the data in object by season and count the players that how many time he won the player of match
        if (result.hasOwnProperty(value.season)) {
            if (result[value.season].hasOwnProperty(value.player_of_match)) {
                result[value.season][value.player_of_match] += 1;
            }
            else {
                result[value.season][value.player_of_match] = 1;
            }
        }
        else {
            result[value.season] = { [value.player_of_match]: 1 };
        }
        return result;
    }, {});
    const highestPlayerOfMatches = Object.keys(data).reduce((result, value) => { // It find the highest player who won the player of the match by earch season
        const max = Math.max.apply(Math, Object.values(data[value]));
        result[value] = Object.entries(data[value]).filter(value => value[1] === max);
        return result;
    }, {});
    return highestPlayerOfMatches;
}
module.exports = highestPlayer;
