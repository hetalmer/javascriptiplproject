//Find the total runs scored by a batsman for each season

function totalRunsByBatsman(matches, deliveries) {
	const idByYear = matches.reduce((result, value) => {
		if (result.hasOwnProperty(value.season)) {
			result[value.season].push(value.id);
		} else {
			result[value.season] = [value.id];
		}
		return result;
	}, {});
	const finalResult = Object.keys(idByYear).reduce((result, values) => {
		const dataBySeason = deliveries.filter((value) => {
			if (idByYear[values].includes(value.match_id)) {
				return values;
			}
		});
		dataBySeason.forEach((element) => {
			if (result.hasOwnProperty(values)) {
				if (result[values].hasOwnProperty(element.batsman)) {
					result[values][element.batsman] += parseInt(element.total_runs);
				} else {
					result[values] = Object.assign({}, result[values], {
						[element.batsman]: parseInt(element.total_runs),
					});
				}
			} else {
				result[values] = { [element.batsman]: parseInt(element.total_runs) };
			}
		});
		return result;
	}, {});
	return finalResult;
}
module.exports = totalRunsByBatsman;
