//5. Find the top 10 batsmen with most fifities
function mostFifty(matches, deliveries) {
	const ids = matches.map((values) => values.id);
	const finalResult = ids.reduce((result, values) => {
		const filterData = deliveries
			.filter((value) => {
				if (value.match_id === values) {
					return value;
				}
			})
			.reduce((acc, data) => {
				if (acc.hasOwnProperty(data.batsman)) {
					acc[data.batsman] += parseInt(data.total_runs);
				} else {
					acc[data.batsman] = parseInt(data.total_runs);
				}
				return acc;
			}, {});

		Object.entries(filterData)
			.filter((value) => value[1] >= 50)
			.forEach((element) => {
				if (result.hasOwnProperty(element[0])) {
					result[element[0]] += 1;
				} else {
					result[element[0]] = 1;
				}
				return result;
			});
		return result;
	}, {});
	return Object.fromEntries(
		Object.entries(finalResult)
			.sort((list1, list2) => list2[1] - list1[1])
			.slice(1, 11),
	);
}
module.exports = mostFifty;
